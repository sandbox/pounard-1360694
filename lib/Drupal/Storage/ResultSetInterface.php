<?php

namespace Drupal\Storage;

use \ArrayAccess;
use \Countable;

interface ResultSetInterface extends Countable, ArrayAccess
{  
}
