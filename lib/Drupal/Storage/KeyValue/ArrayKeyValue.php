<?php

namespace Drupal\Storage\KeyValue;

use Drupal\Storage\StorageInterface;

/**
 * Array value that provides a software based expire feature. It is meant to be
 * used for backends that doesn't support native key expiration and to be stored
 * as serialized objects into those backends.
 *
 * @see Drupal\Storage\ArrayKeyValueStorage
 */
class ArrayKeyValue {
  /**
   * @var int
   */
  protected $expires;

  /**
   * @var mixed
   */
  public $value;

  public function isExpired() {
    if (isset($this->expires)) {
      return time() > $this->expires;
    }
    else {
      return FALSE;
    }
  }

  public function setExpires($expires = StorageInterface::EXPIRES_PERSISTENT) {
    if (!empty($expires)) {
      $this->expires = time() + $expires;
    }
    else {
      $this->expires = NULL;
    }
  }

  public function __construct($value, $expires = StorageInterface::EXPIRES_PERSISTENT) {
    $this->value = $value;
    $this->setExpires($expires);
  }
}
