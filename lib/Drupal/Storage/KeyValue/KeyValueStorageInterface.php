<?php

namespace Drupal\Storage\KeyValue;

use Drupal\Storage\StorageInterface;

/**
 * Key/value storage interface.
 *
 * When you fetch key, be aware that the get() method will return FALSE in case
 * of failure. If you need to store FALSE values please ensure to call exists()
 * before proceeding to get().
 *
 * Generally speaking methods that actually modify values atomically will return
 * the new set value in the result set. Check out method documentation.
 *
 * This will send an additional query, but ensure you fetch was you expected.
 * Any ideas for improving this are welcome.
 * <code>
 * if ($store->exists('some_key')) {
 *   $value = $store->get('some_key');
 *   // Value exists, except in some rare race condition cases.
 * }
 * else {
 *   // No value, do something.
 * }
 * </code>
 *
 * If you want to avoid the query, you can proceed to your get() first, then
 * call the getLastResultStatus() method instead. This will return one of the
 * ResultSet::* constants, in case of fetch fail, an error status will be set
 * there.
 * <code>
 * $value = $store->get('some_key');
 * if (ResultSet::SUCCESS === $store->getLastResultStatus()) {
 *   // Value may be FALSE, but it was stored this way.
 * }
 * else {
 *   // No value, do something else.
 * }
 * </code>
 */
interface KeyValueStorageInterface extends StorageInterface {
  /**
   * Tell if the key exists in current storage.
   *
   * @param string $key
   *
   * @return bool
   */
  public function exists($key);

  /**
   * Set single key.
   *
   * @param string $key
   * @param mixed $value
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function set($key, $value);

  /**
   * Get single key.
   *
   * @param string $key
   *
   * @return mixed
   */
  public function get($key);

  /**
   * Delete single key.
   *
   * @param string $key
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function del($key);

  /**
   * Increment an integer key.
   *
   * If key does not exists, it's created valued with 0, then incremented.
   *
   * If key exists and is not an integer value, ResultSet::FAIL status is set
   * as latest result, else the new value is set.
   *
   * The result set item may be the 0 integer, in order to test failure, you
   * must use the identity operator === which will see the difference between
   * ResultSet::FAIL and the 0 value.
   *
   * @param string $key
   * @param int $value = 1
   *   Absolute value to increment by. Value can be negative.
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function incr($key, $value = 1);

  /**
   * Decrement an integer key.
   *
   * If key does not exists, it's created valued with 0, then decremented.
   *
   * If key exists and is not an integer value, ResultSet::FAIL status is set
   * as latest result, else the new value is set.
   *
   * The result set item may be the 0 integer, in order to test failure, you
   * must use the identity operator === which will see the difference between
   * ResultSet::FAIL and the 0 value.
   *
   * @param string $key
   * @param int $value = 1
   *   Absolute value to decrement by. Value can be negative.
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function decr($key, $value = 1);

  /**
   * Set single key with an expiration delay.
   *
   * @param string $key
   * @param int $expires
   *   Expiry time, in seconds.
   * @param mixed $value = NULL
   *   Optional value that can be passed for insert or update operation.
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function setExpire($key, $expires, $value = NULL);

  /**
   * Get multiple keys at once.
   *
   * This bulk operation will set one result set entry per fetched entry.
   * Failures will be set accordingly, in order.
   *
   * @param array $keys
   *
   * @return array
   *   Key-value pairs. Non existing keys are not present in this array.
   */
  public function getMultiple(array $keys);

  /**
   * Delete multiple keys at once.
   *
   * This aggregate operation populates only on result in result set.
   *
   * @param $keys
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function delMultiple(array $keys);

  /**
   * Set multiple keys at once.
   *
   * This bulk operation will set one result set entry per inserted or updated
   * pair. Failures will be set accordingly, in order.
   *
   * @param array $pairs
   *   Key-value pairs to set.
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function setMultiple(array $pairs);

  /**
   * Set the same expiry time for multiple keys at once.
   *
   * Mutiple set expire does not allow you to set values at the same time.
   *
   * This aggregate operation populates only on result in result set.
   *
   * @param array $keys
   * @param int $expires
   *   Expiry time, in seconds.
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function setExpireMultiple(array $keys, $expires);

  /**
   * Increment all integer keys using the same value.
   *
   * Behavior per key is the same as the incr() method.
   *
   * @param array $keys
   * @param int $value = 1
   *   Absolute value to increment by. Value can be negative.
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function incrMultiple(array $keys, $value = 1);

  /**
   * Decrement all integer keys using the same value.
   *
   * Behavior per key is the same as the decr() method.
   *
   * @param array $keys
   * @param int $value = 1
   *   Absolute value to decrement by. Value can be negative.
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function decrMultiple(array $keys, $value = 1);
}
