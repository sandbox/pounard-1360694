<?php

namespace Drupal\Storage\KeyValue;

use Drupal\Storage\StorageInterface;

use Drupal\Storage\AbstractSimpleStorage;
use Drupal\Storage\DatabaseStorageException;
use Drupal\Storage\ResultSet;

use \PDOException;

/**
 * Database backend uses DBTNG.
 *
 * Sadly, DBTNG is not able to provide async or pipeline support; Which is
 * legit until a certain point: really a few PHP database drivers supports
 * the ASYNC feature (MySQLInd is probably the one and only I know of).
 *
 * Hoping that a bright future will come and DBTNG will support ASYNC feature.
 * If PHP was itself able to ASYNC itself (by threading or any other mean) we
 * would at least simulate this by sending the closure in an async thread,
 * which sadly we can't do.
 *
 * FIXME: Handle internal status.
 */
class DatabaseKeyValueStorage extends AbstractSimpleStorage implements KeyValueStorageInterface {
  /**
   * @var string
   */
  protected $tableName;

  public function exists($key) {
    $exists = (bool)db_query("SELECT 1 FROM {" . $this->tableName . "} WHERE pkey = :pkey AND (expire = 0 OR expire > :time)", array(
      ':pkey' => $key,
      ':time' => time(),
    ))->fetchField();

    if ($exists) {
      $this->setLastResultStatus(ResultSet::SUCCESS);
    }
    else {
      $this->setLastResultStatus(ResultSet::FAIL);
    }

    return $exists;
  }

  public function set($key, $value) {
    $this->setExpire($key, StorageInterface::EXPIRES_PERSISTENT, $value);
    return $this;
  }

  public function get($key) {
    // We won't do any try/catch here, an error happening with this simple
    // select query means that something really wrong happened (database down
    // for example). Let Drupal fail.
    $object = db_query("SELECT * FROM {" . $this->tableName . "} WHERE pkey = :pkey", array(':pkey' => $key))->fetchObject();

    if (!empty($object) && (!$object->expire || $object->expire > time())) {
      $value = unserialize($object->data);
      $this->setLastResultAndStatus($value);
      return $value;
    }
    else {
      // Object does not exists or has expired. We let the garbage collection
      // remove the outdated entry later.
      $this->setLastResultStatus(ResultSet::FAIL);
      return FALSE;
    }
  }

  public function del($key) {
    // We won't do any try/catch here, an error happening with this simple
    // delete query means that something really wrong happened (database down
    // for example). Let Drupal fail.
    db_query("DELETE FROM {" . $this->tableName . "} WHERE pkey = :pkey", array(':pkey' => $key));
    $this->setLastResultStatus(ResultSet::SUCCESS);
    return $this;
  }

  /**
   * Increment an integer key.
   *
   * If key does not exists, it's created valued with 0, then incremented.
   *
   * If key exists and is not an integer value, ResultSet::FAIL status is set
   * as latest result, else the new value is set.
   *
   * The result set item may be the 0 integer, in order to test failure, you
   * must use the identity operator === which will see the difference between
   * ResultSet::FAIL and the 0 value.
   *
   * @param string $key
   * @param int $value = 1
   *   Absolute value to increment by. Value can be negative.
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function incr($key, $value = 1) {
    // FIXME: TODO.
    throw new \RuntimeException("Not implemented yet");
  }

  /**
   * Decrement an integer key.
   *
   * If key does not exists, it's created valued with 0, then decremented.
   *
   * If key exists and is not an integer value, ResultSet::FAIL status is set
   * as latest result, else the new value is set.
   *
   * The result set item may be the 0 integer, in order to test failure, you
   * must use the identity operator === which will see the difference between
   * ResultSet::FAIL and the 0 value.
   *
   * @param string $key
   * @param int $value = 1
   *   Absolute value to decrement by. Value can be negative.
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function decr($key, $value = 1) {
    // FIXME: TODO.
    throw new \RuntimeException("Not implemented yet");
  }

  public function setExpire($key, $expires, $value = NULL) {
    try {
      $result = ResultSet::FAIL;
      $transaction = db_transaction();

      if (empty($expires)) {
        $expires = 0;
      }
      else {
        $expires += time();
      }

      $exists = (bool)db_query("SELECT 1 FROM {" . $this->tableName . "} WHERE pkey = :pkey FOR UPDATE", array(
        ':pkey' => $key,
      ))->fetchField();

      if (isset($value)) {
        if ($exists) {
          db_query("UPDATE {" . $this->tableName . "} SET expire = :expire, data = :data WHERE pkey = :pkey", array(
            ':expire' => $expires,
            ':data' => serialize($value),
            ':pkey' => $key,
          ));
        }
        else {
          db_query("INSERT INTO {" . $this->tableName . "} (pkey, data, expire) VALUES (:pkey, :data, :expire)", array(
            ':pkey' => $key,
            ':data' => serialize($value),
            ':expire' => $expires,
          ));
        }
        $result = ResultSet::SUCCESS;
      }
      else {
        if ($exists) {
          db_query("UPDATE {" . $this->tableName . "} SET expire = :expire WHERE pkey = :pkey", array(
            ':pkey' => $key,
            ':expire' => $expires,
          ));
          $result = ResultSet::SUCCESS;
        }
      }

      // We cannot commit manually, so if the transaction fails, we will display
      // a wrong success here. This case is extremely rare thought.
      $this->setLastResultStatus($result);
    }
    catch (PDOException $e) {
      // We don't know what really happen, so we cannot set the key status.
      $this->setLastResultStatus(ResultSet::FAIL);

      // We don't need rollback since we're doing only one query that alters
      // the data really. In case we have an exception, the update has failed
      // whatever happened.
      throw new DatabaseStorageException($e);
    }

    return $this;
  }

  public function getMultiple(array $keys) {
    // Using the full query builder here that will ensure proper escaping for
    // the IN statement and according placeholders.
    $values = db_select($this->tableName, 't')
      ->fields('t', array('pkey', 'data'))
      ->condition('t.pkey', $keys)
      ->condition(db_or()
        ->condition('t.expire', 0)
        ->condition('t.expire', time(), '>'))
      ->execute()
      ->fetchAllKeyed();
    // We need to populate the result as it must be populated.
    foreach ($keys as $key) {
      if (isset($values[$key])) {
        $values[$key] = unserialize($values[$key]);
        $this->setLastResultAndStatus($values[$key]);
      }
      else {
        $this->setLastResultStatus(ResultSet::FAIL);
      }
    }
    return $values;
  }

  public function delMultiple(array $keys) {
    // Using the full query builder here that will ensure proper escaping for
    // the IN statement and according placeholders.
    db_delete($this->tableName)
      ->condition('pkey', $keys)
      ->execute();
    $this->setLastResultStatus(ResultSet::SUCCESS);
    return $this;
  }

  public function setMultiple(array $keys) {
    foreach ($keys as $key => $value) {
      $this->set($key, $value);
    }
    return $this;
  }

  public function setExpireMultiple(array $keys, $expires) {
    // Using the full query builder here that will ensure proper escaping for
    // the IN statement and according placeholders.
    db_update($this->tableName)
      ->fields(array('expire' => time() + $expires))
      ->condition('pkey', $keys, 'IN')
      ->execute();
    $this->setLastResultStatus(ResultSet::SUCCESS);
    return $this;
  }


  /**
   * Increment all integer keys using the same value.
   *
   * Behavior per key is the same as the incr() method.
   *
   * @param array $keys
   * @param int $value = 1
   *   Absolute value to increment by. Value can be negative.
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function incrMultiple(array $keys, $value = 1) {
    // FIXME: TODO.
    throw new \RuntimeException("Not implemented yet");
  }

  /**
   * Decrement all integer keys using the same value.
   *
   * Behavior per key is the same as the decr() method.
   *
   * @param array $keys
   * @param int $value = 1
   *   Absolute value to decrement by. Value can be negative.
   *
   * @return Drupal\Storage\KeyValue\KeyValueStorageInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function decrMultiple(array $keys, $value = 1) {
    // FIXME: TODO.
    throw new \RuntimeException("Not implemented yet");
  }

  public function garbageCollection() {
    db_delete($this->tableName)
      ->condition('expire', 0, '<>')
      ->condition('expire', time(), '<')
      ->execute();    
  }

  public function __construct($bin_name, array $options = array()) {
    parent::__construct($bin_name, $options);
    $this->tableName = 'keyvalue_' . $this->binName;

    // Prepare connection.
  }
}
