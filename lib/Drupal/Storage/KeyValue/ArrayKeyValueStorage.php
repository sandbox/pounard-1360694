<?php

namespace Drupal\Storage\KeyValue;

use Drupal\Storage\AbstractSimpleStorage;
use Drupal\Storage\ResultSet;

/**
 * Sample implementation of key value store using an array.
 *
 * This implementation only exists as a foolproof implementation behaving as
 * intended: it aims to help for building test suite for other backends, and
 * can be used as a straight forward implementation example.
 *
 * This implementation uses serial calls to atomic read or update methods when
 * doing bulk read or updates, this is only for sample purpose. Any specific
 * backend implementation that supports bulk operations must use it in order
 * for these methods to have a real meaning.
 */
class ArrayKeyValueStorage extends AbstractSimpleStorage implements KeyValueStorageInterface {
  /**
   * @var array
   */
  protected $data = array();

  protected function existsAndNotExpired($key) {
    if (isset($this->data[$key])) {
      if ($this->data[$key]->isExpired()) {
        unset($this->data[$key]);
      }
      else {
        return TRUE;
      }
    }
    return FALSE;
  }

  public function exists($key) {
    if ($this->existsAndNotExpired($key)) {
      $this->setLastResultStatus(ResultSet::SUCCESS);
      return TRUE;
    }
    else {
      $this->setLastResultStatus(ResultSet::FAIL);
      return FALSE;
    }
  }

  public function set($key, $value) {
    $this->data[$key] = new ArrayKeyValue($value);
    $this->setLastResultStatus(ResultSet::SUCCESS);
    return $this;
  }

  public function get($key) {
    if ($this->existsAndNotExpired($key)) {
      $value = $this->data[$key]->value;
      $this->setLastResultAndStatus($value);
      return $value;
    }
    else {
      $this->setLastResultStatus(ResultSet::FAIL);
    }
    return FALSE;
  }

  public function del($key) {
    unset($this->data[$key]);
    $this->setLastResultStatus(ResultSet::SUCCESS);
    return $this;
  }

  public function incr($key, $value = 1) {
    if ($this->existsAndNotExpired($key)) {
      $valueObject = $this->data[$key];
      if (is_int($valueObject->value)) {
        $valueObject->value += (int)$value;
        $this->setLastResultAndStatus($valueObject->value);
      }
      else {
        $this->setLastResultStatus(ResultSet::FAIL);
      }
    }
    else {
      $this->data[$key] = new ArrayKeyValue((int)$value);
      $this->setLastResultAndStatus((int)$value);
    }
    return $this;
  }

  public function decr($key, $value = 1) {
    if ($this->existsAndNotExpired($key)) {
      $valueObject = $this->data[$key];
      if (is_int($valueObject->value)) {
        $valueObject->value -= (int)$value;
        $this->setLastResultAndStatus($valueObject->value);
      }
      else {
        $this->setLastResultStatus(ResultSet::FAIL);
      }
    }
    else {
      $this->data[$key] = new ArrayKeyValue(0 - $value);
      $this->setLastResultAndStatus(0 - $value);
    }
    return $this;
  }

  public function setExpire($key, $expires, $value = NULL) {
    $result = ResultSet::SUCCESS;
    if (isset($value)) {
      $this->data[$key] = new ArrayKeyValue($value, $expires);
    }
    else {
      if ($this->existsAndNotExpired($key)) {
        $this->data[$key]->setExpires($expires);
      }
      else {
        $result = ResultSet::FAIL;
      }
    }
    $this->setLastResultStatus($result);
    return $this;
  }

  public function getMultiple(array $keys) {
    $values = array();
    foreach ($keys as $key) {
      if ($this->existsAndNotExpired($key)) {
        $values[$key] = $this->data[$key]->value;
        $this->setLastResultAndStatus($values[$key]);
      }
      else {
        $this->setLastResultStatus(ResultSet::FAIL);
      }
    }
    return $values;
  }

  public function delMultiple(array $keys) {
    foreach ($keys as $key) {
      unset($this->data[$key]);
    }
    $this->setLastResultStatus(ResultSet::SUCCESS);
    return $this;
  }

  public function setMultiple(array $keys) {
    foreach ($keys as $key => $value) {
      $this->set($key, $value);
    }
    return $this;
  }

  public function setExpireMultiple(array $keys, $expires) {
    foreach ($keys as $key) {
      if ($this->existsAndNotExpired($key)) {
        $this->data[$key]->setExpires($expires);
      }
    }
    $this->setLastResultStatus(ResultSet::SUCCESS);
    return $this;
  }

  public function incrMultiple(array $keys, $value = 1) {
    foreach ($keys as $key) {
      $this->incr($key, $value);
    }
    return $this;
  }

  public function decrMultiple(array $keys, $value = 1) {
    foreach ($keys as $key) {
      $this->decr($key, $value);
    }
    return $this;
  }

  public function garbageCollection() {
    foreach ($this->data as $key => $value) {
      if ($value->isExpired()) {
        unset($this->data[$key]);
      }
    }
  }
}
