<?php

namespace Drupal\Storage;

interface StorageType {
  /**
   * Key value storage.
   */
  const KeyValue = 'keyvalue';
}
