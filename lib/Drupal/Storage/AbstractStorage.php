<?php

namespace Drupal\Storage;

abstract class AbstractStorage implements StorageInterface {
  /**
   * @var string
   */
  protected $binName;

  /**
   * Component options injected at construct time.
   *
   * @var array
   */
  protected $options;

  /**
   * Last internal result fetched.
   *
   * @var array
   */
  protected $lastResultSet = array();

  /**
   * Last internal result status.
   *
   * @var int
   */
  protected $lastResultStatus = ResultSet::NIL;

  public function getLastResultSet() {
    if (!empty($this->lastResultSet)) {
      return new ArrayResultSet($this->lastResultSet);
    }
  }

  public function getLastResultStatus() {
    return $this->lastResultStatus;
  }

  /**
   * Set last result and result status using the same value.
   *
   * @param int $status
   */
  protected function setLastResultStatus($status) {
    $this->lastResultStatus = $status;
    $this->lastResultSet[] = $status;
  }

  /**
   * Set last result and result status.
   *
   * @param int $status
   * @param mixed $result
   */
  protected function setLastResultAndStatus($result, $status = ResultSet::SUCCESS) {
    $this->lastResultStatus = $status;
    $this->lastResultSet[] = $result;
  }

  public function reset() {
    $this->lastResultStatus = ResultSet::NIL;
    $this->lastResultSet = array();
  }

  /**
   * Get bin name.
   *
   * @return string
   */
  public function getBinName() {
    return $this->binName;
  }

  /**
   * Construct new component.
   *
   * @param string $bin_name
   * @param array $options
   */
  public function __construct($bin_name, array $options = array()) {
    $this->binName = $bin_name;
    $this->options = $options;
  }
}
