<?php

namespace Drupal\Storage;

use \RuntimeException as SplException;

class UnknownStorageTypeException extends SplException implements StorageException
{
  public function __construct($storage_type) {
    parent::__construct($storage_type . " storage type does not exist");
  }
}
