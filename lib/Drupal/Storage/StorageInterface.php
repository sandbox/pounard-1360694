<?php

namespace Drupal\Storage;

/**
 * Basic shared behaviors for all storage related components.
 */
interface StorageInterface {
  /**
   * Never expire value.
   */
  const EXPIRES_PERSISTENT = NULL;

  /**
   * Get bin name.
   *
   * @return string
   */
  public function getBinName();

  /**
   * Process the given callback sending all commands asynchroneously. Commands
   * sent asynchroneously cannot be guaranteed to work, and their result cannot
   * be fetched.
   *
   * @param callback $callback
   *   A callback that accepts this same storage interface component as first
   *   parameter. This parameter must be used inside the function to send the
   *   various commands that needs to run in async mode. Some backends may
   *   change the instance and send you a fake one instead.
   *
   * @throws Drupal\Storage\StorageException
   */
  public function doAsync($callback);

  /**
   * Proecss the given callback sending all commands pipelined. Pipelined
   * commands results can be fetched later using the getLastResultSet() method.
   *
   * @param callback $callback
   *   A callback that accepts this same storage interface component as first
   *   parameter. This parameter must be used inside the function to send the
   *   various commands that needs to run in async mode. Some backends may
   *   change the instance and send you a fake one instead.
   *
   * @throws Drupal\Storage\StorageException
   */
  public function doPipelined($callback);

  /**
   * Get last result set.
   *
   * @return Drupal\Storage\ResultSetInterface
   *
   * @throws Drupal\Storage\StorageException
   */
  public function getLastResultSet();

  /**
   * Get last result status.
   *
   * In opposition to getLastResultSet(), this function does not return values
   * fetched, in order to be able to differenciate NULL and FALSE set value to
   * failures.
   *
   * @return mixed
   *   ResultSet::FAIL in case of failure, ResultSet::SUCCESS in case of success.
   */
  public function getLastResultStatus();

  /**
   * Kill internal result status and result set. This might be useful if you
   * need to do consistency checks over a non transactional chaining result
   * set.
   */
  public function reset();

  /**
   * Manually run garbage collection of expired entries.
   *
   * Backends don't need to implement this as soon as the backend supports the
   * expiry time for entries.
   */
  public function garbageCollection();

  /**
   * Construct new component.
   *
   * @param string $bin_name
   * @param array $options
   */
  public function __construct($bin_name, array $options = array());
}
