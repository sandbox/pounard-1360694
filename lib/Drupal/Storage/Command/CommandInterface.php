<?php

namespace Drupal\Storage\Command;

interface CommandInterface
{
  /**
   * Parse result from command.
   *
   * @param mixed $data
   *   Raw result from backend, specific to backend.
   *
   * @return mixed
   */
  public function parseResult($data);
}
