<?php

namespace Drupal\Storage\Command;

/**
 * Provide a default implementation of storage that will take care of async and
 * pipelined sending in a straightforward and generic way.
 *
 * Most backends are more than advise not to implement this interface except if
 * they don't support aync and pipelined transactions.
 */
abstract class AbstractCommandStorage extends AbstractStorage {
  /**
   * Last internal result fetched.
   *
   * @var array
   */
  protected $lastResultSet;

  /**
   * @var bool
   */
  protected $inTransaction = FALSE;

  /**
   * Start internal transaction.
   */
  protected function startTransaction() {
    if (!$this->inTransaction) {
      $this->lastResultSet = array();
      $this->inTransaction = TRUE;
    }
  }

  /**
   * Stop internal transaction.
   */
  protected function endTransaction() {
    $this->inTransaction = FALSE;
  }

  /**
   * Switch on the backend in asynchroneous mode.
   *
   * Default behavior is no-op, this is a graceful downgrade and compatibility
   * support for backends that don't support this feature.
   */
  protected function enableAsync() {}

  /**
   * Switch off the backend asynchroneous mode.
   *
   * Default behavior is no-op, this is a graceful downgrade and compatibility
   * support for backends that don't support this feature.
   */
  protected function disableAsync() {}

  /**
   * Switch on the backend in pipelined mode.
   *
   * Default behavior is no-op, this is a graceful downgrade and compatibility
   * support for backends that don't support this feature.
   */
  protected function enablePipeline() {
    $this->startTransaction();
  }

  /**
   * Switch off the backend pipelined mode.
   *
   * Default behavior is no-op, this is a graceful downgrade and compatibility
   * support for backends that don't support this feature.
   */
  protected function commitPipeline() {
    $this->endTransaction();
  }

  /**
   * Generic command send and populate the internal result set.
   *
   * @param CommandInterface $command
   *
   * @return mixed
   *   Raw result from the command.
   *
   * @throws Drupal\Storage\StorageException
   */
  protected function send(CommandInterface $command) {
    if ($this->inTransaction) {
      $this->lastResultSet[] = $command->parseResult($this->sendToBackend($command));
    }
    else {
      return $command->parseResult($this->sendToBackend($command));
    }
  }

  /**
   * Really send given command to backend. Do not worry about parsed result
   * and just return the backend raw result.
   *
   * @param CommandInterface $command
   *
   * @return mixed
   *   Raw server result.
   */
  protected abstract function sendToBackend(CommandInterface $command);

  public function doAsync($callback) {
    $this->enableAsync();

    try {
      $callback($this);
    }
    catch (StorageException $e) {
      $this->disableAsync();
      throw $e;
    }

    $this->disableAsync();
  }

  public function doPipelined($callback) {
    $this->enablePipeline();

    try {
      $callback($this);
    }
    catch (StorageException $e) {
      $this->commitPipeline();
      throw $e;
    }

    $this->commitPipeline();
  }

  public function getLastResultSet() {
    if (isset($this->lastResultSet)) {
      return new ArrayResultSet($this->lastResultSet);
    }
  }
}
