<?php

namespace Drupal\Storage;

use \ArrayObject;

/**
 * Array based ResultSetInterface implementation.
 */
class ArrayResultSet extends ArrayObject implements ResultSetInterface
{
}
