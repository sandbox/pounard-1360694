<?php

namespace Drupal\Storage;

use \Exception;

/**
 * Provide a default implementation of storage that will take care of async and
 * pipelined sending in a straightforward and generic way.
 *
 * Most backends are more than advise not to implement this interface except if
 * they don't support aync and pipelined transactions.
 */
abstract class AbstractSimpleStorage extends AbstractStorage {
  /**
   * @var bool
   */
  protected $inTransaction = FALSE;

  /**
   * Start internal transaction.
   *
   * During a transaction, all results are aggregated into the internal result
   * set in order to be fetched later, once the transaction ended.
   */
  protected function startTransaction() {
    if (!$this->inTransaction) {
      $this->reset();
      $this->inTransaction = TRUE;
    }
  }

  /**
   * Stop internal transaction.
   */
  protected function endTransaction() {
    $this->inTransaction = FALSE;
  }

  /**
   * Switch on the backend in asynchroneous mode.
   *
   * Default behavior is no-op, this is a graceful downgrade and compatibility
   * support for backends that don't support this feature.
   */
  protected function enableAsync() {}

  /**
   * Switch off the backend asynchroneous mode.
   *
   * Default behavior is no-op, this is a graceful downgrade and compatibility
   * support for backends that don't support this feature.
   */
  protected function disableAsync() {}

  /**
   * Switch on the backend in pipelined mode.
   *
   * Default behavior is no-op, this is a graceful downgrade and compatibility
   * support for backends that don't support this feature.
   */
  protected function enablePipeline() {
    $this->startTransaction();
  }

  /**
   * Switch off the backend pipelined mode.
   *
   * Default behavior is no-op, this is a graceful downgrade and compatibility
   * support for backends that don't support this feature.
   */
  protected function commitPipeline() {
  }

  public function doAsync($callback) {
    if (!is_callable($callback)) {
      throw new Exception("Provided callback is not callable()");
    }

    $this->enableAsync();

    try {
      $callback($this);
    }
    catch (StorageException $e) {
      $this->disableAsync();
      throw $e;
    }

    $this->disableAsync();
  }

  public function doPipelined($callback) {
    if (!is_callable($callback)) {
      throw new Exception("Provided callback is not callable()");
    }

    $this->enablePipeline();

    try {
      $callback($this);
    }
    catch (StorageException $e) {
      $this->commitPipeline();
      throw $e;
    }

    $this->commitPipeline();
  }
}
