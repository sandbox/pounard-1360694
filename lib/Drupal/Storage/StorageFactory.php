<?php

namespace Drupal\Storage;

/**
 * Helper that will replace a dependency injection container in order to use
 * Drupal configuration system instead.
 */
class StorageFactory {
  /**
   * Singleton pattern implementation.
   *
   * @var Drupal\Storage\StorageInstance
   */
  private static $instance;

  /**
   * Get instance.
   *
   * @return Drupal\Storage\StorageFactory
   */
  public static function getInstance() {
    if (!isset(self::$instance)) {
      self::$instance = new self();
    }
    return self::$instance;
  }

  /**
   * Singleton pattern implementation.
   */
  private function __construct() {}

  /**
   * Link living instances.
   *
   * @var array
   */
  protected $backends = array();

  /**
   * Get storage component instance from configuration options.
   *
   * FIXME: This is definitely ugly code.
   *
   * @param string $storage_type
   *   Storage type.
   * @param string $bin_name
   *   Storage bin name.
   * @param string $default_class = NULL
   *   Default class to use if none found.
   * @param array $default_options = array()
   *   Options to fallback with if default backend is used.
   */
  protected function getInstanceByConf($storage_type, $bin_name, $default_class = NULL, $default_options = array()) {
    $conf = variable_get('storage');
    $conf = $conf[$storage_type];

    // Try first with specific bin name, then default one.
    foreach (array($bin_name, 'default') as $metabin) {
      // We need to check the class exists first.
      if (isset($conf[$metabin]['backend']) && class_exists($conf[$metabin]['backend'])) {

        $class = $conf[$metabin]['backend'];

        if (isset($conf[$metabin]['backend']['options'])) {
          $options = $conf[$metabin]['backend']['options'];
        }
        else {
          $options = array();
        }

        return new $class($bin_name, $options);
      }
    }

    // Reaching here mean we either don't have configuration for the backend,
    // either the class specified does not exists.
    if (isset($default_class)) {
      if (class_exists($default_class)) {
        return new $default_class($bin_name, $default_options);
      }
      else {
        throw new RuntimeException("Class " . $default_class . " does not exist");
      }
    }
    else {
      throw new RuntimeException("Unable to find a component class to use for specified storage bin");
    }
  }

  /**
   * Get storage instance for given bin and given type.
   *
   * @param string $bin_name
   * @param string $storage_type
   *
   * @return Drupal\Storage\StorageInterface
   *
   * @throws Drupal\Storage\UnknownStorageTypeException
   */
  public function getStorage($bin_name, $storage_type = StorageType::KeyValue) {
    switch ($storage_type) {

      case StorageType::KeyValue:
        if (!isset($this->backends[$storage_type][$bin_name])) {
          $this->backends[$storage_type][$bin_name] = $this->getInstanceByConf($storage_type, $bin_name, 'DatabaseKeyValueStorage');
        }
        return $this->backends[$storage_type][$bin_name];

      default:
        throw new UnknownStorageTypeException($storage_type);
    }
  }
}
