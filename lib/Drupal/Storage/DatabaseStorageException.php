<?php

namespace Drupal\Storage;

use \Exception;
use \RuntimeException;

class DatabaseStorageException extends RuntimeException implements StorageException
{
  /**
   * Default constructor.
   *
   * @param Exception $innerException
   */
  public function __construct(Exception $innerException) {
    parent::__construct("Caught a database error", 0, $innerException);
  }
}
