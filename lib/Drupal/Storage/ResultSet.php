<?php

namespace Drupal\Storage;

/**
 * Result set constants.
 */
abstract class ResultSet
{
  /**
   * Operation gives no return.
   */
  const NIL = NULL;

  /**
   * Operation failed.
   */
  const FAIL = FALSE;

  /**
   * Operation succeded.
   */
  const SUCCESS = TRUE;
}
